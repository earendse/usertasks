package za.co.jcode.blackswan.usertasks.persistance;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import za.co.jcode.blackswan.usertasks.dto.impl.Task;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface TaskRepository extends CrudRepository<Task, Long> {

    @Query("select t from Task t where t.userId = :userId")
    List<Task> findTaskForUser(@Param("userId") Long userId);

    @Query("select t from Task t where t.id = :id and t.userId = :userId")
    Task findTaskByUserId(@Param("id") Long id, @Param("userId") Long userId);

    @Query("delete from Task t where t.id = :id and t.userId = :userId")
    void deleteTaskByIdAndUserId(@Param("id") Long id, @Param("userId") Long userId);

    void deleteTaskById(Long id);

}
