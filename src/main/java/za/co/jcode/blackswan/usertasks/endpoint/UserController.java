package za.co.jcode.blackswan.usertasks.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import za.co.jcode.blackswan.usertasks.UserNotFoundException;
import za.co.jcode.blackswan.usertasks.dto.impl.User;
import za.co.jcode.blackswan.usertasks.persistance.UserRepository;
import javax.ws.rs.core.MediaType;
import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/user")
public class UserController {

    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserRepository userDTORepository ;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test(){
        return "This is a test";
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<User> addUser(@RequestBody User user){
         logger.debug("Adding user: " + user);
         User newUser = this.userDTORepository.save(user);
         logger.debug("got new user: " + newUser);

         URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                 .path("/{id}").buildAndExpand(newUser.getId()).toUri();


         return ResponseEntity.created(location).build();
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON)
    public List<User> getUser(){
        return userDTORepository.findAll();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
    public User findOne(@PathVariable("id") Long id) {

        if(!userDTORepository.existsById(id)){
            throw new UserNotFoundException(id);
        }

        User user = this.userDTORepository.findById(id).orElse(null);
        logger.debug("Found user: " + user);
        return user;
    }

    @PutMapping(value = "/{id}")
    public User updateUser(@RequestBody User user, @PathVariable("id") Long id){

        if(!userDTORepository.existsById(id)){
            throw new UserNotFoundException(id);
        }

        user.setId(id);
        User updatedUser = this.userDTORepository.save(user);

        logger.debug("Update user: " + updatedUser);

        return updatedUser;
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Long id){

        if(!userDTORepository.existsById(id)){
            throw new UserNotFoundException(id);
        }

        this.userDTORepository.deleteById(id);
    }


}
