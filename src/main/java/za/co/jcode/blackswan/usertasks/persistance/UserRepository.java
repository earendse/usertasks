package za.co.jcode.blackswan.usertasks.persistance;

import org.springframework.data.repository.CrudRepository;
import za.co.jcode.blackswan.usertasks.dto.impl.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {

    List<User> findAll();

    //boolean exists

}
