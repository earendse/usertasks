package za.co.jcode.blackswan.usertasks.dto;

import java.util.Date;

public interface ITask {

    String getDescription();
    void setDescription(String description);

    Long getId();
    void setId(Long id);

    String getName();
    void setName(String name);

    Date getDate();
    void setDate(Date date);

    void setUserId(Long id);
    Long getUserId();
}
