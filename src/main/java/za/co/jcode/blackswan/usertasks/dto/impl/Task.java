package za.co.jcode.blackswan.usertasks.dto.impl;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.jcode.blackswan.usertasks.dto.ITask;

import javax.persistence.*;
import java.util.Date;

@Entity
@ToString
@NoArgsConstructor
public class Task implements ITask {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date_time;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, name = "user_id")
    private Long userId;

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    @JsonGetter("date_time")
    public Date getDate() {
        return date_time;
    }

    @Override
    @JsonSetter("date_time")
    public void setDate(Date date) {
        date_time = date;
    }

    @Override
    public void setUserId(Long id) {
        userId = id;
    }

    @Override
    public Long getUserId() {
        return userId;
    }
}

