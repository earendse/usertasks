package za.co.jcode.blackswan.usertasks.dto.impl;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.jcode.blackswan.usertasks.dto.IUser;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@ToString
@JsonIgnoreProperties(value={"id"}, allowGetters = true)
@Entity
@Table(name="user")
public class User implements IUser {

    @GeneratedValue
    @Id
    private Long id;

    @NotNull
    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String first_name;

    @Column(nullable = false)
    private String last_name;

    @Override
    public void setUsername(final String username) {
        this.username = username;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    @JsonSetter("first_name")
    public void setFirstName(final String name) {
        first_name = name;
    }

    @Override
    @JsonGetter("first_name")
    public String getFirstName() {
        return first_name;
    }

    @Override
    @JsonSetter("last_name")
    public void setLastName(String lastname) {
        last_name = lastname;
    }

    @Override
    @JsonGetter("last_name")
    public String getLastname() {
        return last_name;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

}
