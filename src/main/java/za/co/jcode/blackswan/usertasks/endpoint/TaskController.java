package za.co.jcode.blackswan.usertasks.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import za.co.jcode.blackswan.usertasks.TaskNotFoundException;
import za.co.jcode.blackswan.usertasks.UserNotFoundException;
import za.co.jcode.blackswan.usertasks.dto.impl.Task;
import za.co.jcode.blackswan.usertasks.persistance.TaskRepository;
import za.co.jcode.blackswan.usertasks.persistance.UserRepository;

import javax.ws.rs.core.MediaType;
import java.util.List;


@RestController
@RequestMapping("/user/{userId}/task")
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserRepository userRepository;

    private static Logger logger = LoggerFactory.getLogger(TaskController.class);

    @PostMapping(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public Task addTask(@RequestBody Task task, @PathVariable("userId") Long userId){

        if(userRepository.existsById(userId)){
            throw new UserNotFoundException(userId);
        }

        task.setUserId(userId);
        logger.debug("Before db: " + task);
        Task newTask = taskRepository.save(task);
        logger.debug("Before db: " + newTask);

        return newTask;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
    public Task getTask(@PathVariable Long id, @PathVariable Long userId) {

        if(!userRepository.existsById(userId)){
            throw new UserNotFoundException(userId);
        } else if(!taskRepository.existsById(id)){
            throw new TaskNotFoundException(id, userId);
        }

        return this.taskRepository.findTaskByUserId(id, userId);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON)
    public List<Task> getTaskListForUser(@PathVariable("userId") Long userId) {

        if(!userRepository.existsById(userId)){
            throw new UserNotFoundException(userId);
        }

        return this.taskRepository.findTaskForUser(userId);
    }

    @DeleteMapping(value = "/{id}")
    public HttpStatus deleteTask(@PathVariable("userId") Long userId, @PathVariable("id") Long id) {

        if(!userRepository.existsById(userId)){
            throw new UserNotFoundException(userId);
        } else if(!taskRepository.existsById(id)){
            throw new TaskNotFoundException(id, userId);
        }

        this.taskRepository.deleteTaskById(id);

        return HttpStatus.OK;
    }


    @PutMapping(value ="/{id}")
    public HttpStatus updateTask(@RequestBody Task task, @PathVariable("userId") Long userId, @PathVariable("id") Long id) {

        if(!userRepository.existsById(userId)){
            throw new UserNotFoundException(userId);
        } else if(!taskRepository.existsById(id)){
            throw new TaskNotFoundException(id, userId);
        }

        task.setUserId(userId);
        task.setId(id);

        logger.debug("Task: " + task);

        this.taskRepository.save(task);

        return HttpStatus.NO_CONTENT;
    }
}
