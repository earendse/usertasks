package za.co.jcode.blackswan.usertasks.dto;

public interface IUser {

    void setUsername(String username);
    String getUsername();

    void setFirstName(String name);
    String getFirstName();

    void setLastName(String lastname);
    String getLastname();

    void setId(Long id);
    Long getId();

}
