package za.co.jcode.blackswan.usertasks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("za.co.jcode.blackswan.usertasks.dto.impl")
@EnableJpaRepositories("za.co.jcode.blackswan.usertasks.persistance")
public class UsertasksApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsertasksApplication.class, args);
	}
}
