package za.co.jcode.blackswan.usertasks;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.jcode.blackswan.usertasks.dto.impl.User;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UsertasksApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UsertasksApplicationTests {

	@LocalServerPort
	private int port;

	private User createUser(String username, String fname, String sname){

		User userDTO = new User();
		userDTO.setUsername("earendse");
		userDTO.setLastName(sname);
		userDTO.setFirstName(fname);

		return userDTO;
	}

	private URI createURI(String endpoint) throws URISyntaxException {
		return new URI("http://localhost:" + port + endpoint);
	}

	private void deleteUser(URI url) throws Exception {
		TestRestTemplate restTemplate = new TestRestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>(null, new HttpHeaders());

		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.DELETE, entity, String.class);
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));

	}

	@Test
	public void addUser() throws Exception {
		TestRestTemplate restTemplate = new TestRestTemplate();

		User userDTO = createUser("earendse", "elizabeth", "arendse");

		HttpEntity<User> entity = new HttpEntity<>(userDTO, new HttpHeaders());

		ResponseEntity<User> response = restTemplate.exchange(createURI("/api/user"), HttpMethod.POST, entity, User.class);

		assertEquals(response.getStatusCode(), HttpStatus.CREATED);
		deleteUser(response.getHeaders().getLocation());

	}

	@Test
	public void deleteUser_does_not_exist() throws URISyntaxException {
		TestRestTemplate restTemplate = new TestRestTemplate();

		HttpEntity<String> entity = new HttpEntity<String>(null, new HttpHeaders());

		ResponseEntity<String> response = restTemplate.exchange(createURI("/api/user/2"), HttpMethod.DELETE, entity, String.class);
		assertTrue(response.getStatusCode().equals(HttpStatus.NOT_FOUND));
	}

}